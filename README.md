# Mini SSO

The goal of Mini SSO (single sign-on) is to develop a protocol for single sign-on. Previous
protocols like [SAML](https://en.wikipedia.org/wiki/Security_Assertion_Markup_Language "Security
Assertion Markup Language"), or [OpenID Connect](https://en.wikipedia.org/wiki/OpenID_Connect)
are very flexible and include features like authorization, but flexibility and features come at a
price: Those protocols are complex and often need very specific configuration for interoperability.

## Goals

Mini SSO follows a different philosophy. The main goals are:

* Simple Design
* High Security
* Good Interoperability with Little Effort

## Features

* Single Sign-on
* Assigning Users to User Groups
* Single Sign-out (optional)
* Pass User Name and Password from Service Provider to Identity Provider (optional; for systems
    that do not support http redirects; opinions on this?)

This is still a work in progress, so features can and should be discussed. However, the goal is to
keep things simple. Therefore the feature list will not get extended without good reason. In
circumstances where sophisticated features are needed one should use more sophisticated protocols.

## Protocol

The current draft of the
[Mini SSO protocol is here](https://bitbucket.org/mini-sso/mini-sso-protocol/src/master/PROTOCOL.md).

## Contributing

At the moment I prefer not to accept pull requests. In case this protocol becomes popular, it would
be easier to get it a standard (e. g. an RFC). Nevertheless I would enjoy discussions on how to
improve this protocol to better meet the needs of the users etc. Feel free to open issues or join
the discussion in issues other users opened.

## License

Copyright 2018 Florian Will

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
